# HCI 2018
GERON (Gestural Remote Control)

![alt text](https://bitbucket.org/alexandru_tetelea/hci-2018/raw/b6837e1b0dbc4ab18c7967328a8414d3095d9357/Design/Scholarly%20HTML%20Report/images/logo.png)


A smart personal assistant which makes use of natural means of interaction � for example,
gestural, (pseudo-)haptic � in order to control certain home appliances such as BluRay players, fridges, 
kitchen robots, washing machines, TV sets, and others.
For a proper interaction, propose a set of gestures to be used to control these (classes of) home appliances. 
The status of each controlled appliance can be viewed via a smart device (phone, tablet, TV) interface.


�  Geron can be accessed here (using Chrome browser): https://geron2018.herokuapp.com

�  Visit our blog: https://geronapp.wordpress.com

�  Usability Test Report: https://goo.gl/S53Bwv

�  Scholarly HTML Techincal Report: http://students.info.uaic.ro/~alexandru.tetelea/geron/

�  Scholarly HTML Design Report: http://students.info.uaic.ro/~alexandru.tetelea/hci/Scholarly%20HTML%20Report/


Team members

� Tetelea Alexandru (alexandru.tetelea@gmail.com)

� Lupu Anisia-Ioana (anisia.lupu@yahoo.com)

