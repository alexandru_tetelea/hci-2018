var app = angular.module('myApp', ['ngAnimate', 'ngAnimate']);

app.controller('myCtrl', function ($scope, $timeout, $http, $log) {
    $scope.devices = [];
    $timeout(function () {
        $http({
            method: 'GET',
            url: '/api/devices'
        }).then(function successCallback(response) {
            $scope.devices = response.data
        }, function errorCallback(response) {
            console.log(response)
        });
    });

    send_command = function (command) {
        let data = {"value": command}
        $timeout(function () {
            $http({
                method: 'POST',
                url: '/command',
                data: data,
            }).then(function successCallback(response) {
                console.log(response)
            }, function errorCallback(response) {
                console.log(response)
            });
        });
    };

    $scope.submitCommand = function () {
        send_command(this.commandText);

    };
    var socket = new SockJS('/websocket-endpoint');
    stompClient = Stomp.over(socket);
    var last_executed;
    let say_command = function (command) {
        artyom.say(command, {
            onStart: function () {
                // Don't obey any command
                artyom.dontObey();
            },
            onEnd: function () {
                setTimeout(function () {
                    // Allow to process commands again
                    artyom.obey();
                }, 5000);// wait 2 seconds
            }
        });
    };
    startConnection = function () {
        stompClient.connect({}, function (frame) {
            console.log('Connected: ' + frame);
            stompClient.subscribe('/global-message/update', function (payload) {
                payload = JSON.parse(payload.body);
                $timeout(function () {
                    let device = $scope.devices.find(function (element) {
                        return element.id === payload.id;
                    });
                    let index = $scope.devices.indexOf(device);
                    if (index !== -1) {
                        if (payload.removed !== undefined) {
                            $scope.devices.splice(index, 1);
                            $.notify(payload.deviceName + ' was removed !', "info");
                            say_command("Device " + payload.deviceName + " is removed !")
                        } else {
                            if (device.status !== payload.status) {
                                device.status = payload.status;
                                $.notify(payload.deviceName + ' is ' + payload.status, "info");
                                say_command(payload.deviceName + " is " + payload.status + " !")
                            } else {
                                say_command(payload.deviceName + " already was " + payload.status + "!")
                            }

                        }
                    } else {
                        $.notify(payload.deviceName + ' was added!', "info");
                        say_command("Device " + payload.deviceName + " was added !")
                        $scope.devices.unshift(payload);
                    }

                })
            });
        });
    };
    startConnection();
    socket.onclose = function () {
        startConnection()
    };

    let artyom = new Artyom();
    artyom.on(['* now'], true).then((i, wildcard) => {
        send_command(wildcard);
    });

    artyom.on(['on *'], true).then((i, wildcard) => {
        send_command("on " + wildcard);
    });

    artyom.on(['off *'], true).then((i, wildcard) => {
        send_command("off " + wildcard);
    });

    artyom.on(['stop *'], true).then((i, wildcard) => {
        send_command("stop " + wildcard);
    });

    artyom.on(['kill your*'], true).then((i, wildcard) => {
        send_command("Lide is cruel! " + wildcard);
    });

    artyom.on(['remove *'], true).then((i, wildcard) => {
        send_command("remove " + wildcard);
    });

    artyom.on(['Hi G*', "Hi J*", "hi geron*", "hi g*", "hi j*", "Haji*", "hey j*", "hey G*", "haiji*", "hi Darren*", "Darren*"], true).then((i, wildcard) => {
        artyom.say("How I can help you?");
    });

    artyom.on(['add *'], true).then((i, wildcard) => {
        send_command("add " + wildcard);
    });

    artyom.on(['start *'], true).then((i, wildcard) => {
        send_command("start " + wildcard);
    });

    artyom.on(['turn *'], true).then((i, wildcard) => {
        send_command(wildcard);
    });


    // artyom.on(['stop *'], true).then((i, wildcard) => {
    //     send_command(wildcard);
    //     artyom.say("Done");
    // });
    //
    // artyom.on(['start *'], true).then((i, wildcard) => {
    //     send_command(wildcard);
    //     artyom.say("Done");
    // });
    //
    // artyom.on(['shutdown *'], true).then((i, wildcard) => {
    //     send_command(wildcard);
    //     artyom.say("Done");
    // });
    //
    // artyom.on(['on *'], true).then((i, wildcard) => {
    //     send_command(wildcard);
    //     artyom.say("Done");
    // });
    //
    // artyom.on(['off *'], true).then((i, wildcard) => {
    //     send_command(wildcard);
    //     artyom.say("Done");
    // });

    artyom.initialize({
        lang: "en-GB", // GreatBritain english
        continuous: true, // Listen forever
        soundex: true,// Use the soundex algorithm to increase accuracy
        debug: true, // Show messages in the console
        executionKeyword: "now",
        listen: true // Start to listen commands !
    }).then(() => {
        console.log("Artyom has been succesfully initialized");
    }).catch((err) => {
        console.error("Artyom couldn't be initialized: ", err);
    });
});