package ro.geron.core;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ro.geron.data.models.Device;

@Repository
public interface DevicesRepository extends MongoRepository<Device, Long> {
}
