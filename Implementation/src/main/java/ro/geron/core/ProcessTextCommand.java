package ro.geron.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.geron.commands.AddDeviceCommand;
import ro.geron.commands.RemoveDeviceCommand;
import ro.geron.commands.TurnOffDeviceCommand;
import ro.geron.commands.TurnOnDeviceCommand;
import ro.geron.data.models.Device;
import ro.geron.data.models.RemovedDevice;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class ProcessTextCommand implements Processor<String, Device> {
    public static Map<String, List<String>> devicesImages = new HashMap<>();

    static {
        devicesImages.put("coffe", Arrays.asList("https://asda.scene7.com/is/image/Asda/5054070280423?hei=532&wid=910&qlt=85&fmt=pjpg&resmode=sharp&op_usm=1.1,0.5,0,0&defaultimage=default_details_George_rd"));
        devicesImages.put("washing", Arrays.asList("https://brain-images-ssl.cdn.dixons.com/9/8/10140689/u_10140689.jpg"));
        devicesImages.put("phone", Arrays.asList("https://i-cdn.phonearena.com/images/phones/42885-xlarge/Apple-iPhone-5s-0.jpg"));
        devicesImages.put("tv", Arrays.asList("http://images.samsung.com/is/image/samsung/my-full-hd-k6300-ua49k6300akxxm-001-front-black?$PD_GALLERY_L_JPG$"));
        devicesImages.put("default", Arrays.asList("https://cdn1.iconfinder.com/data/icons/computer-and-internet/512/electronic_devices_computer_tablet_smartphone-512.png"));
    }

    private final DevicesRepository repository;
    private final TurnOffDeviceCommand offDeviceCommand;
    private final TurnOnDeviceCommand onDeviceCommand;
    private final RemoveDeviceCommand removeDeviceCommand;
    private final AddDeviceCommand addDeviceCommand;
    private final StringSimilarity stringSimilarity;
    List<String> stopCommand = Arrays.asList("off", "stop", "down", "stutdown", "close");
    List<String> startCommand = Arrays.asList("on", "start", "up", "play", "open");

    @Autowired
    public ProcessTextCommand(DevicesRepository repository, TurnOffDeviceCommand offDeviceCommand, TurnOnDeviceCommand onDeviceCommand, RemoveDeviceCommand removeDeviceCommand, AddDeviceCommand addDeviceCommand, StringSimilarity stringSimilarity) {
        this.repository = repository;
        this.offDeviceCommand = offDeviceCommand;
        this.onDeviceCommand = onDeviceCommand;
        this.removeDeviceCommand = removeDeviceCommand;
        this.addDeviceCommand = addDeviceCommand;
        this.stringSimilarity = stringSimilarity;
    }

    @Override
    public Device process(String value) {
        List<Device> deviceList = repository.findAll();
        if (value.contains("already")) {
            return null;
        }
        if (value.contains("all")) {
            deviceList.forEach(it -> executeLocal(value, it));
        }
        if (value.contains("add")) {
            Device device = new Device();
            int last = value.indexOf("now");
            last = last == -1 ? value.length() : last;
            device.deviceName = value.substring(value.indexOf("add") + 3, last);
            if (device.image == null) {
                device.image = getDeviceImage(device.deviceName);
            }
            addDeviceCommand.execute(repository.save(device));
            return null;
        }


        getDevices(deviceList, value).stream().forEach(it -> executeLocal(value, it));
        return null;
    }

    private List<Device> getDevices(List<Device> deviceList, String value) {
        int first = value.indexOf("on");
        int start = first + 2;

        if (first == -1) {
            first = value.indexOf("off");
            start = first + 3;
        }

        if (first == -1) {
            first = value.indexOf("start");
            start = first + 4;
        }

        if (first == -1) {
            first = value.indexOf("stop");
            start = first + 4;
        }

        if (first == -1) {
            first = value.indexOf("play");
            start = first + 4;
        }
        int last = value.indexOf("now");
        last = last == -1 ? value.length() : last;
        String deviceName = value.substring(start, last);
        List<Device> exact = Optional.of(deviceName)
                .filter(it -> !stopCommand.contains(it))
                .filter(it -> !startCommand.contains(it))
                .filter(it -> deviceList.stream().map(d -> d.deviceName).anyMatch(d -> match(d, it)))
                .map(it -> deviceList.stream().filter(d -> d.deviceName.equalsIgnoreCase(it)).collect(Collectors.toList())).orElse(Collections.emptyList());
        if (exact.size() > 0) {
            return exact;
        }

        List<Device> general = Stream.of(value.split("\\W+"))
                .filter(it -> !stopCommand.contains(it))
                .filter(it -> !startCommand.contains(it))
                .filter(it -> deviceList.stream().map(d -> d.deviceName).anyMatch(d -> match(d, it)))
                .map(it -> deviceList.stream().filter(d -> match(d.deviceName, it)).collect(Collectors.toList()))
                .flatMap(List::stream)
                .distinct()
                .collect(Collectors.toList());
        return general;
    }

    private void executeLocal(String value, Device devices) {
        if (startCommand.stream().anyMatch(it -> Stream.of(value.split("\\W+")).anyMatch(l -> l.equalsIgnoreCase(it)))) {
            devices.status = "on";
            repository.save(devices);
            onDeviceCommand.execute(devices);
        } else if (stopCommand.stream().anyMatch(it -> Stream.of(value.split("\\W+")).anyMatch(l -> l.equalsIgnoreCase(it)))) {
            devices.status = "off";
            repository.save(devices);
            offDeviceCommand.execute(devices);
        } else if (value.contains("remove")) {
            repository.delete(devices);
            removeDeviceCommand.execute(new RemovedDevice(devices));
        }
    }

    private String getDeviceImage(String name) {
        List<String> cat = devicesImages.keySet().stream().filter(it -> match(name, it)).collect(Collectors.toList());
        String category = cat.size() > 0 ? cat.get(0) : "default";
        return devicesImages.get(category).get(0);
    }

    private boolean match(String fromUser, String fromDb) {
        return Stream.of(fromUser.split("\\W+"))
                .filter(it -> it != null && !it.equals(""))
                .anyMatch(it -> Stream.of(fromDb.split("\\W+")).anyMatch(l -> stringSimilarity.similarity(l, it) > 0.7));
    }
}
