package ro.geron.core;

public interface Processor<T,R> {
    R process(T value);
}
