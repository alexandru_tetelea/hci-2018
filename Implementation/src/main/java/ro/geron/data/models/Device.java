package ro.geron.data.models;

import lombok.*;
import org.springframework.data.annotation.Id;

import java.util.List;

@ToString
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Device {
    public String category;
    @Id
    public String id;
    public String image;
    public String deviceName;
    public List<String> tags;
    public String status;
}
