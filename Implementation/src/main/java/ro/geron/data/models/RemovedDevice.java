package ro.geron.data.models;

public class RemovedDevice extends Device {
    public Boolean removed;

    public RemovedDevice(Device device) {
        this.id = device.id;
        this.removed = true;
        this.deviceName = device.deviceName;
        this.status = device.status;
    }
}
