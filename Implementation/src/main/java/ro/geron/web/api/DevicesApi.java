package ro.geron.web.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;
import ro.geron.core.DevicesService;
import ro.geron.data.models.Device;

@RestController
@RequestMapping("/api/devices")
@CrossOrigin
public class DevicesApi {
    private final DevicesService devicesService;

    @Autowired
    public DevicesApi(DevicesService devicesService) {
        this.devicesService = devicesService;
    }

    @GetMapping
    public Iterable<Device> getDevices() {
        return devicesService.findAll();
    }

    @GetMapping
    @RequestMapping("/{page}/{count}")
    public Iterable<Device> getDevices(@PathVariable("page") Integer page, @PathVariable("count") Integer count) {
        return devicesService.findAll(gotoPage(page, count));
    }


    @PostMapping
    public Device addDevice(@RequestBody Device device) {
        return devicesService.save(device);
    }


    private PageRequest gotoPage(int page, int count) {
        PageRequest request = PageRequest.of(page, count, Sort.Direction.ASC, "id");
        return request;
    }
}
